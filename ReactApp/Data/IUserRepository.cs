﻿using ReactApp.Models;

namespace ReactApp.Data
{
    public interface IUserRepository
    {
        User Create(User user);
        UserCredentials Create(UserCredentials user);
        UserDetails Create(UserDetails user);
        UserPermissions Create(UserPermissions user);

        void Update(User user);

        User GetUserById(int id);
        UserCredentials GetUserCredentialsByEmail(string email);
        UserDetails GetUserDetailsById(int id);

        bool UpdateUserAvatar(int id, string avatarname);

        void SaveChanges();
    }
}
