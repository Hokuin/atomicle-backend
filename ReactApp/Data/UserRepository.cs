﻿using ReactApp.Models;
using System.Linq;

namespace ReactApp.Data
{
    public class UserRepository : IUserRepository
    {
        private readonly AtomicleContext context;

        public UserRepository(AtomicleContext context)
        {
            this.context = context;
        }

        public User Create(User user)
        {
            context.Users.Add(user);
            //user.userID = context.SaveChanges();
            return user;
        }

        public void Update(User user)
        {
            context.Users.Update(user);
        }

        public UserCredentials Create(UserCredentials userCredentials)
        {
            context.UserCredentials.Add(userCredentials);
            //userCredentials.ID = context.SaveChanges();
            return userCredentials;
        }

        public UserDetails Create(UserDetails userDetails)
        {
            context.UserDetails.Add(userDetails);
            //userDetails.ID = context.SaveChanges();
            return userDetails;
        }

        public UserPermissions Create(UserPermissions userPermissions)
        {
            context.UserPermissions.Add(userPermissions);
            //userPermissions.ID = context.SaveChanges();
            return userPermissions;
        }

        public User GetUserById(int id)
        {
            return context.Users.Where(t => t.userID == id).FirstOrDefault();
        }

        public UserCredentials GetUserCredentialsByEmail(string email)
        {
            return context.UserCredentials.Where(t => t.email == email).FirstOrDefault();
        }

        public UserDetails GetUserDetailsById(int id)
        {
            return context.UserDetails.Where(t => t.userID == id).FirstOrDefault();
        }

        public bool UpdateUserAvatar(int id, string avatarname)
        {
            var user = GetUserById(id);
            if (user == null) return false;

            user.profileImage = avatarname;
            return true;
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }
    }
}
